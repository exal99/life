class World(object):
    def __init__(self, cols, rows, default=False, ):
        
        self.num_rows = rows
        self.num_cols = cols

        self.cells = set()

    def populate(self, pattern=["  ***", " *  *", "*   *"]):
        if not pattern: return

        pattern_rows = len(pattern)
        pattern_cols = max([len(pattern[i]) for i in range(len(pattern))])

        start_x = int(self.num_cols/2 - pattern_cols/2)
        start_y = int(self.num_rows/2 - pattern_rows/2)
        for y in range(pattern_rows):
            for x in range(pattern_cols):
                if not pattern[y][x] == ' ':
                    self.alive(start_x + x, start_y + y)



    def get_num_rows(self):
        return self.num_rows


    def get_num_columns(self):
        return self.num_cols


    def is_alive(self, col, row):
        return (col, row) in self.cells


    def alive(self, col, row):
        self.cells.add((col, row))


    def dead(self, col, row):
        self.cells.discard((col, row))
        
        
    def resize(self, cols, rows):
        new_cells = set()
        
        if cols < self.num_cols:
            range_x = cols
            offset_x_new = 0
            offset_x_old = self.num_cols//2 - cols//2
        else:
            range_x = self.num_cols
            offset_x_new = cols//2 - self.num_cols//2
            offset_x_old = 0
            
        if rows < self.num_rows:
            range_y = rows
            offset_y_new = 0
            offset_y_old = self.num_rows//2 - rows//2
        else:
            range_y = self.num_rows
            offset_y_new = rows//2 - self.num_rows//2
            offset_y_old = 0
        
        for x in range(range_x):
            for y in range(range_y):
                if self.is_alive(x + offset_x_old, y + offset_y_old):
                    new_cells.add((x + offset_x_new, y + offset_y_new))
        self.num_cols = cols
        self.num_rows = rows
        self.cells = new_cells
        
        
    def __eq__(self, other):
        if type(other) is type(self):
            if isinstance(other, self.__class__):
                return self.__dict__ == other.__dict__
        return False

    def __ne__(self, other):
        return not self.__eq__(other)

